CREATE DATABASE  IF NOT EXISTS `ntt_ml_lg` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ntt_ml_lg`;
-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ntt_ml_lg
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ntt_data_excel`
--

DROP TABLE IF EXISTS `ntt_data_excel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ntt_data_excel` (
  `Numero_de_venta` varchar(45) DEFAULT NULL,
  `Fecha_de_venta` varchar(45) DEFAULT NULL,
  `Estado` varchar(45) DEFAULT NULL,
  `Descripcion_del_estado` varchar(100) DEFAULT NULL,
  `Paquete_de_varios_productos` varchar(45) DEFAULT NULL,
  `Unidades` int DEFAULT NULL,
  `Ingresos_por_productos_(COP)` varchar(45) DEFAULT NULL,
  `Ingresos_por_envío_(COP)` varchar(45) DEFAULT NULL,
  `Cargo_por_venta_e_impuestos` varchar(45) DEFAULT NULL,
  `Costos_de_envio` varchar(45) DEFAULT NULL,
  `Anulaciones_y_reembolsos_(COP)` varchar(45) DEFAULT NULL,
  `Total_(COP)` varchar(45) DEFAULT NULL,
  `Facturacion` varchar(45) DEFAULT NULL,
  `SKU` varchar(45) NOT NULL,
  `Numero_de_publicacion` varchar(45) NOT NULL,
  `Tienda_Oficial` varchar(45) DEFAULT NULL,
  `Titulo_de_la_publicacion` varchar(300) DEFAULT NULL,
  `Variante` varchar(45) DEFAULT NULL,
  `Precio_unitario_de_venta_de_la_publicacion_(COP)` varchar(45) DEFAULT NULL,
  `Tipo_de_publicacion` varchar(45) DEFAULT NULL,
  `Comprador` varchar(100) DEFAULT NULL,
  `CC` varchar(25) DEFAULT NULL,
  `Domicilio` varchar(250) DEFAULT NULL,
  `Municipio_o_ciudad_capital` varchar(45) DEFAULT NULL,
  `Estado_o_Capital` varchar(45) DEFAULT NULL,
  `Codigo_postal` varchar(45) DEFAULT NULL,
  `Pais` varchar(45) DEFAULT NULL,
  `Forma_de_entrega_Envios` varchar(60) DEFAULT NULL,
  `Fecha_en_camino_Envios` varchar(45) DEFAULT NULL,
  `Fecha_entregado_Envios` varchar(45) DEFAULT NULL,
  `Transportista_Envio` varchar(45) DEFAULT NULL,
  `Numero_de_seguimiento_Envio` varchar(45) DEFAULT NULL,
  `URL_de_seguimiento_Envios` varchar(500) DEFAULT NULL,
  `Forma_de_entrega_Devoluciones` varchar(45) DEFAULT NULL,
  `Fecha_en_camino_Devoluciones` varchar(45) DEFAULT NULL,
  `Fecha_entregado_Devoluciones` varchar(45) DEFAULT NULL,
  `Transportista_Devoluciones` varchar(45) DEFAULT NULL,
  `Numero_de_seguimiento_Devoluciones` varchar(45) DEFAULT NULL,
  `URL_de_seguimiento_Devoluciones` varchar(45) DEFAULT NULL,
  `Reclamo_abierto` varchar(45) DEFAULT NULL,
  `Reclamo_cerrado` varchar(45) DEFAULT NULL,
  `Con_mediacion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`SKU`,`Numero_de_publicacion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ntt_data_excel`
--

LOCK TABLES `ntt_data_excel` WRITE;
/*!40000 ALTER TABLE `ntt_data_excel` DISABLE KEYS */;
INSERT INTO `ntt_data_excel` VALUES ('5049405207','26 de noviembre de 2021 13:06 hs.','Entregado','Llegó el 3 de diciembre','No',1,'729900','','-97006.12','-8850','','624043.88','Factura no adjunta','22MN430H-B','MCO808599439','LG Electronics Colombia','Monitor LG Panel 22mn430h-b 22 Pulgadas Full Hd Amd Freesync',' ','729900','Clásica','Juan Sebastian Ariza Jimenez','1110587892','Carrera 83 B #31-37 / Referencia: APTO 201, Edificio Leblon - Belén Los Alpes, Medellín, Antioquia','Medellín','Antioquia','050026','Colombia','Correo y puntos de despacho','30 de noviembre | 11:37','3 de diciembre | 14:10','Envia','024019201126','https://portal.envia.co/OnLineRastreo/Rastreo/Rastreo?Guia=024019201126#modal_rastrea',' ',' ',' ',' ',' ',' ','No','1','No'),('5026165458','17 de noviembre de 2021 17:46 hs.','Cancelaste la venta','El comprador hizo un reclamo porque no tienes stock disponible.','No',1,'941109','','-131786.98','-8850','-800472.02','0','Factura no adjunta','29WP60G','MCO808560560','LG Electronics Colombia','Monitor LG 29wp60g Ips Ultrawide  Full Hd 29 Pulgadas',' ','941109','Clásica','Jose Luis Quevedo Casanova','620085',' ','Envigado','Antioquia','055422','Colombia','Correo y puntos de despacho',' ',' ','Envia','024019154229',' ',' ',' ',' ',' ',' ',' ','No','1','No'),('5089017800','10 de diciembre de 2021 15:34 hs.','Etiqueta lista para imprimir','Tienes que despachar el paquete hoy o el lunes en Envia.','No',1,'2749900','','-219992','-19500','','2510408','Factura no adjunta','55NANO75SPA','MCO832882527','LG Electronics Colombia','Tv LG Nanocell 55\' 4k Smart Tv 5 Ai Processor',' ','2749900','Clásica','Diego Cote Santamaria','1007363828','Diagonal 18 15 125 #15-125 / Hotel La Orquidea - Juan Vargas, Barbosa, Santander','Barbosa','Santander','684511','Colombia','Correo y puntos de despacho',' ',' ','Envia','024019270876',' ',' ',' ',' ',' ',' ',' ','No','','No'),('5078835161','6 de diciembre de 2021 16:24 hs.','Cancelaste la venta','El comprador hizo un reclamo porque lo encontró a mejor precio.','No',1,'2499900','','-300072.07','-19500','-2180327.93','0','Factura no adjunta','55UP7500PSF','MCO809909982','LG Electronics Colombia','Tv LG 55  139 Cm 55up7500psf 4k-uhd Led Plano Smart Tv',' ','2499900','Clásica','Daniel Bañol','1088266020',' ','Engativá','Bogotá D.C.','111031','Colombia','Correo y puntos de despacho',' ',' ','Envia','024019251510',' ',' ',' ',' ',' ',' ',' ','No','1','No'),('5066034787','1 de diciembre de 2021 23:58 hs.','Llega entre el 13 y 16 de diciembre','El paquete está en viaje nuevamente.','Si',1,'4181000','','-501860.59','-31500','','3647639.41','Factura no adjunta','75NANO75SPA','MCO832812595','LG Electronics Colombia','Tv LG Nanocell 75\' 4k Smart Tv 5 Ai Processor',' ','4181000','Clásica','Mark Gil','1125659601','Carrera 10 #10-60 / Chiminango, Bolívar, Valle Del Cauca','Bolívar','Valle Del Cauca',' ','Colombia','Correo y puntos de despacho','4 de diciembre | 13:10',' ','Envia','024019227871','https://portal.envia.co/OnLineRastreo/Rastreo/Rastreo?Guia=024019227871#modal_rastrea',' ',' ',' ',' ',' ',' ','No','','No'),('5095475756','11 de diciembre de 2021 20:30 hs.','Esperando disponibilidad de stock','Tienes hasta el 13 de diciembre para terminar el producto.','No',1,'5599900','','-784174.27','-31500','','4784225.73','Factura no adjunta','GS77SPP','MCO835184719','LG Electronics Colombia','Refrigerador LG','Color : Gris','5599900','Clásica','DAVID LEONARDO AMADOR ALMAZO','1082906973','carrera 30 #SN-SN / manzana f casa 36 - ciudad del sol 3, Santa Marta, Magdalena','Santa Marta','Magdalena','470003','Colombia','Correo y puntos de despacho',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ','No','','No'),('5075119672','5 de diciembre de 2021 13:32 hs.','En camino','Llega entre el 9 y 14 de diciembre','No',1,'829900','','-128808.58','-5700','','695391.42','Factura no adjunta','RN5','MCO833289893','LG Electronics Colombia','Torre De Sonido LG Xboom Rn5/ Bluetooth','Color : Negro | Voltaje : 110V','829900','Clásica','Cindy Alvarez Gomez','1039456989','Calle 5 #08-75 / Unidad campestre Paradiso, casa 79 - El barro, Girardota, Antioquia','Girardota','Antioquia','051030','Colombia','Correo y puntos de despacho','7 de diciembre | 11:42',' ','Envia','024019248394','https://portal.envia.co/OnLineRastreo/Rastreo/Rastreo?Guia=024019248394#modal_rastrea',' ',' ',' ',' ',' ',' ','No','','No'),('5076398338','5 de diciembre de 2021 22:05 hs.','En camino','Llega el lunes 20 de diciembre','No',1,'429900','','-77396.39','-13500','','339003.61','Factura no adjunta','SP2','MCO833247499','LG Electronics Colombia','Barra De Sonido LG Sp2 Negro','Color : Negro | Voltaje : 110V','429900','Clásica','miguel molina','1090478306','Carrera #3-125 / Frente colegio caldas - Miraflores, Tibú, Norte De Santander','Tibú','Norte De Santander','548010','Colombia','Correo y puntos de despacho','7 de diciembre | 11:42',' ','Envia','024019248282','https://portal.envia.co/OnLineRastreo/Rastreo/Rastreo?Guia=024019248282#modal_rastrea',' ',' ',' ',' ',' ',' ','No','','No'),('5109138117','15 de diciembre de 2021 18:44 hs.','Etiqueta lista para imprimir','Tienes que despachar el paquete hoy o mañana en Envia.','No',1,'7699900','','-1078244.85','-31500','','6590155.15','Factura no adjunta','WK22VS6','MCO839488558','LG Electronics Colombia','LG Washtower Lavadora (22kg/48lbs) & Secadora (22kg/48lbs)','Color : Gris','7699900','Clásica','Juan Pablo Parra','80577836','Vía 4 esquinas #SN-SN / Referencia: Vereda Tiquiza, sector Flores Valvanera, Camino escuela Fagua, Predio Brujas. Condominio Portobari Casa 78 - Vereda Tiquiza, sector Flores Valvanera, Camino escuela Fagua, Predio Brujas, Chía, Cundinamarca','Chía','Cundinamarca',' ','Colombia','Correo y puntos de despacho',' ',' ','Envia','024019300423',' ',' ',' ',' ',' ',' ',' ','No','','No');
/*!40000 ALTER TABLE `ntt_data_excel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ntt_ml_lg_control`
--

DROP TABLE IF EXISTS `ntt_ml_lg_control`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ntt_ml_lg_control` (
  `id_venta` varchar(20) NOT NULL,
  `po_generada` int DEFAULT NULL,
  `ubicacion_po` varchar(800) DEFAULT NULL,
  `fecha_hora_generacion_po` varchar(30) DEFAULT NULL,
  `po_enviada_lg` int DEFAULT NULL,
  `fecha_hora_enviada_po` varchar(30) DEFAULT NULL,
  `facturado_alegra` int DEFAULT NULL,
  `numero_factura` varchar(50) DEFAULT NULL,
  `fecha_hora_facturacion_alegra` varchar(30) DEFAULT NULL,
  `descargado_alegra` int DEFAULT NULL,
  `ubicacion_factura_descargada` varchar(800) DEFAULT NULL,
  `fecha_hora_descarga_factura` varchar(30) DEFAULT NULL,
  `factura_cargada_ml` int DEFAULT NULL,
  `fecha_hora_cargue_ml` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_venta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ntt_ml_lg_control`
--

LOCK TABLES `ntt_ml_lg_control` WRITE;
/*!40000 ALTER TABLE `ntt_ml_lg_control` DISABLE KEYS */;
INSERT INTO `ntt_ml_lg_control` VALUES (' 5049405207 ',1,' https://docs.google.com/spreadsheets/d/1_Ic0B3HHNx-5lOT6Xlq6iQSjWb8-VjWX/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','26/11/2021 02:18:57',1,'26/11/2021 02:18:57',1,'FE3','26-11-2021 02:18:57',1,'C:\\Users\\Usuario\\Documents\\Repositorio_Local_RPA_ML_LG\\Control_Seguimiento\\TMP_INVOICES\\ 5049405207','09/12/2021 06:39:18',1,'09/12/20 07:05:25'),(' 5066034787 ',1,' https://docs.google.com/spreadsheets/d/1h1Sqoge6NyVpkGjPSXjPlDNfnSs4yxks/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','02/12/2021 09:38:58',1,'02/12/2021 09:38:58',1,'FE8','13-12-2021 03:36:45',1,NULL,NULL,NULL,NULL),(' 5075119672 ',1,' https://docs.google.com/spreadsheets/d/1e3L_APJX77I5WREf3i7xA8wsJKaa6bwj/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','06/12/2021 01:16:49',1,'06/12/2021 01:16:49',1,'FE6','13-12-2021 04:14:14',1,NULL,NULL,NULL,NULL),(' 5076398338 ',1,'https://docs.google.com/spreadsheets/d/10qSXTqvcT_ONRuKmCh3sxP1mn1cNf0Rm/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true','06/12/2021 12:44:27',1,'06/12/2021 12:44:27',1,'FE5','13-12-2021 03:42:45',NULL,NULL,NULL,NULL,NULL),(' 5078835161 ',1,' https://docs.google.com/spreadsheets/d/1H_XtXYPTbMwl74NhTYH48bs6agRbtslf/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','06/12/2021 05:12:11',1,'06/12/2021 05:12:11',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(' 5089017800 ',1,' https://docs.google.com/spreadsheets/d/1vgHN9_qdGWB5BgvYWMYJUT4Q_s7OcDUz/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','10/12/2021 04:46:26',1,'10/12/2021 04:46:26',1,'FE9','20-12-2021 08:34:28',NULL,NULL,NULL,NULL,NULL),(' 5095475756 ',1,' https://docs.google.com/spreadsheets/d/14A-lZJvyhqulfBd9RD4snrsQ8Hr-j7E_/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','13/12/2021 09:43:05',1,'13/12/2021 09:43:05',1,'FE10','20-12-2021 09:53:07',NULL,NULL,NULL,NULL,NULL),(' 5109138117 ',1,' https://docs.google.com/spreadsheets/d/1b72QgoOGxQ3QWQ3500M9HVAbo9hy_4n2/edit?usp=sharing&ouid=115318367710714257752&rtpof=true&sd=true ','16/12/2021 07:11:35',1,'16/12/2021 07:11:35',1,'FE11','20-12-2021 10:17:13',NULL,NULL,NULL,NULL,NULL),('5026165458',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `ntt_ml_lg_control` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ntt_ml_lg_po_counters`
--

DROP TABLE IF EXISTS `ntt_ml_lg_po_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ntt_ml_lg_po_counters` (
  `DATE_CRT` varchar(30) NOT NULL,
  `ORDERS` int NOT NULL,
  `INVENTORY_RESET` int NOT NULL,
  PRIMARY KEY (`DATE_CRT`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ntt_ml_lg_po_counters`
--

LOCK TABLES `ntt_ml_lg_po_counters` WRITE;
/*!40000 ALTER TABLE `ntt_ml_lg_po_counters` DISABLE KEYS */;
INSERT INTO `ntt_ml_lg_po_counters` VALUES ('02-12-2021',1,0),('06-12-2021',3,0),('10-12-2021',1,0),('13-12-2021',1,0),('16-12-2021',2,0),('20-11-2021',1,0),('26-11-2021',1,0);
/*!40000 ALTER TABLE `ntt_ml_lg_po_counters` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-20 16:12:43
